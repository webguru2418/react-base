import axios from 'axios';
// import constants from 'common/utils/constants';
import { BASEURL } from '../../config/endpoints/api';
import { getToken } from '../../Auth';
const axiosClient = axios.create();
axiosClient.defaults.baseURL = BASEURL;

// axiosClient.defaults.headers = constants.headers;
axiosClient.defaults.headers = {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                };
// axios.defaults.headers.common['Authorization'] = "6531|qoobrUOvQR6pgPB2hRnC2UPgbgx9iBAmnG18Zg9i";
axiosClient.interceptors.request.use((config) => {
    const token = getToken();
      if (token) {
          config.headers = { Authorization: `Bearer ${token}` };
      } else { 
          config.headers = { Authorization: null } 
      }
      return config;
    },
    (error) => {
      return Promise.reject(error);
    }
);

// To share cookies to cross site domain, change to true.
axiosClient.defaults.withCredentials = false;
axiosClient.defaults.timeout = 5000;

export function getRequest(URL) {
  return axiosClient.get(`${URL}`).then(response => response);
}

export function getCustomRequest(URL) {
  return axiosClient.get(`${URL}`).then(response => response);
}

export function posttRequest(URL, payload) {
  return axiosClient.post(`${URL}`, payload).then(response => response);
}

export function postRequest(URL, payload, headers = {}) {
  // return axiosClient.post(`${URL}`, payload).then(response => response);
  return apiRequest(URL, payload, "post", headers);
}

export function patchRequest(URL, payload) {
  return axiosClient.patch(`${URL}`, payload).then(response => response);
}

export function deleteRequest(URL) {
  return axiosClient.delete(`${URL}`).then(response => response);
}