import {all, call, put, takeLatest} from 'redux-saga/effects'
import actions from './actions';
import { auth } from '../../app/config/endpoints/api';
import { setOnLocalStorage, removeFromLocalStorage } from '../../utils/localStorage';;
import {postRequest, posttRequest, getCustomRequest, getRequest, deleteRequest} from '../../app/httpClient/axiosClient'

function* login(action) {
  try {
    // yield call(() => getCustomRequest('sanctum/csrf-cookie'));
    // const response = yield call(() => postRequest('login', action.payload));
    const response = yield call(() => postRequest(auth.login, action.payload));
    if( response.status === 200 ){
      setOnLocalStorage('token', response.data.token);
      yield put({type: actions.LOGIN_SUCCESS, payload: response});
    }
  } catch (error) {
    console.log({error});
    yield put({type: actions.LOGIN_FAILURE});
    if( error.code === 'ERR_NETWORK' ){
      console.log(error.message);
      return false;
    } else if ( error.code === 'ECONNABORTED' ){
      console.log(error.message);
      return false;
    }
    if(error.response.status === 401) {
      console.log(error.response.data.message)
    } else if(error.response.status === 405) {
      console.log("Invalid request method")
    } else {
      // message.error('Something Went Wrong');
      console.log('Something Went Wrong')
    }
  }
}

function* getAuthUser() {
  try {
    const response = yield call(() => getRequest('auth/user'));
    yield put({type: actions.GET_AUTH_USER_SUCCESS, payload: response.data});
  } catch (error) {
    yield put({type: actions.GET_AUTH_USER_FAILURE});
  }
}



export default function* rootSaga() {
  yield all([takeLatest(actions.LOGIN, login)]);
  yield all([takeLatest(actions.GET_AUTH_USER, getAuthUser)]);
}
