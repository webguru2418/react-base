import actions from './actions';
import { getFromLocalStorage } from '../../utils/localStorage';

const initialState = {
  isAuthenticated: getFromLocalStorage('token') ? true:false,
  loader: false,
  email: null,
  name: null,
  validateUserLoader: true,
  logOutLoader: false,
  // token: getFromLocalStorage('token') ? getFromLocalStorage('token'):null
}

function Reducer(state = initialState, action) {
  switch (action.type) {
    case actions.GET_AUTH_USER:
      return {...state, validateUserLoader: true}
    case actions.LOGIN:
      return {...state, loader: true}
    default:
      return state
  }
}

export default Reducer;
