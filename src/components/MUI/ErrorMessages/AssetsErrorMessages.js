import {TableCell, TableRow, Typography} from '@mui/material';
function AssetsErrorMessages(props){
    return props.errorMessage !== "" ? <>
        <TableRow sx={{ border: 0 }}>
            <TableCell sx={{ border: 0 }} colSpan={3} className="tax-items" align='left'>
                <Typography className="assets-error-message" component="span">
                    {props.errorMessage}
                </Typography>
            </TableCell>
        </TableRow>
    </>
    :false
}

export default AssetsErrorMessages;