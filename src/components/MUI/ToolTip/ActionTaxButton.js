import React, {useState} from "react";
import {Typography, Tooltip} from "@mui/material";
import AddCircleTwoToneIcon from '@mui/icons-material/AddCircleTwoTone';
import HighlightOffTwoToneIcon from '@mui/icons-material/HighlightOffTwoTone';

function ActionTaxButton(props){
    const isLastRow = props.rowNo === props.NoOfRows ? true:false;
    return  <>
        <Tooltip 
            onClick={props.onClick} 
            title={isLastRow ? props.title:'Remove row'} 
            placement={props.placement} 
            arrow>
            <Typography className={`span-add-new-line`} component="span">
                { isLastRow ? <AddCircleTwoToneIcon />:<HighlightOffTwoToneIcon /> }
            </Typography>
        </Tooltip>
    </>
}

export default ActionTaxButton;